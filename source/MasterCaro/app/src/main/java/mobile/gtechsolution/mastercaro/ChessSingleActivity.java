package mobile.gtechsolution.mastercaro;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ChessSingleActivity extends Activity {

    private  ChessView myChess;
    boolean isEnd = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chess_single_activity);
        myChess = (ChessView)findViewById(R.id.chessView);
        myChess.setOnFinishGameEvenListener(new ChessView.OnFinishGameEvenListener() {
            @Override
            public void onFinishGame() {
                isEnd = true;
            }
        });

    }

    public void callUndo(View v)
    {
        if(!isEnd)
            myChess.UndoMoves();
    }
    public void callNewGame(View v)
    {
        myChess.resetMatrixState();
        isEnd = false;
    }
    public void callHome(View v)
    {
        finish();
    }
    public void callHint(View v)
    {
        if(!isEnd)
            myChess.hintPlayer();
    }
}
