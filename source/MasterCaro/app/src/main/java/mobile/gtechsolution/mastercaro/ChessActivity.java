package mobile.gtechsolution.mastercaro;

import android.app.Activity;
import android.os.Bundle;

public class ChessActivity extends Activity {

    private  ChessView myChess;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chess_activity);
        myChess = (ChessView)findViewById(R.id.chessView);

    }
}
